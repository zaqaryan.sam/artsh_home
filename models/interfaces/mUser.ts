import { ObjectId, Document } from 'mongoose';

export interface UserModel extends Document{
  _id?: ObjectId,
  name: string,
  email: string,
  age: number,
}

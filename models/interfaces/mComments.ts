import { ObjectId, Document } from 'mongoose';

export interface CommentModel extends Document{
  _id?: ObjectId,
  author: ObjectId,
  post_id: ObjectId,
  text: string,
  likes: number,
  parent?:ObjectId,
}

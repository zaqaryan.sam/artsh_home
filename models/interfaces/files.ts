import { ObjectId, Document } from 'mongoose';

export interface FilesDocument extends Document{
  _id?: ObjectId,
  author: ObjectId,
  url: ObjectId,
  type: string,
}

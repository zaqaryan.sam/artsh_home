import { ObjectId, Document } from 'mongoose';

export interface RelationsDocument extends Document{
  _id?: ObjectId,
  author: ObjectId,
  target: ObjectId,
  age: number,
  type:string,
  date:number,
}

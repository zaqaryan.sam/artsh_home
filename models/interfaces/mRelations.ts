import { ObjectId, Document } from 'mongoose';

export interface RelationsModel extends Document{
  _id?: ObjectId,
  author: ObjectId,
  target: ObjectId,
  age: number,
  type:string,
  date:number,
}

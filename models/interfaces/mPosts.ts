import { ObjectId, Document } from 'mongoose';

export interface PostsModel extends Document{
  _id?: ObjectId,
  author: ObjectId,
  text: string,
  likes: number,
  comment:number,
  images:ObjectId,
  Videos:ObjectId,
  origin: ObjectId
  parent:ObjectId
}

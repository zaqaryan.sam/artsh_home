import { ObjectId, Document } from 'mongoose';

export interface UserDocument extends Document{
  _id?: ObjectId,
  name: string,
  email: string,
  age: number,
}

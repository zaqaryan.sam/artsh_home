import { ObjectId, Document } from 'mongoose';

export interface PostsDocument extends Document{
  _id?: ObjectId,
  author: ObjectId,
  text: string,
  likes: number,
  comment:number,
  images:ObjectId,
  Videos:ObjectId,
  origin: ObjectId
  parent:ObjectId
}

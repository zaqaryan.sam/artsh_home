import { ObjectId, Document } from 'mongoose';

export interface FilesModel extends Document{
  _id?: ObjectId,
  author: ObjectId,
  url: ObjectId,
  type: string,
}

import { model } from 'mongoose';

import { UserDocument } from './interfaces/user';

import { userSchema } from './schemes/user';

export const models = {
  users: model<UserDocument>('users', userSchema, 'users'),
};

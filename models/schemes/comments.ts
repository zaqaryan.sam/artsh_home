import { Schema, Types } from 'mongoose';

export const commentsSchema = new Schema({
  _id: {
    type: Types.ObjectId,
    auto: true,
  },
  author: {
    type: Types.ObjectId,
  },
  postId: {
    type: Types.ObjectId,
  },
  text: {
    type: String,
  },
  likes: {
    type: Number,
  },
  parent: {
    type: Types.ObjectId,
  },
});

import { Schema, Types } from 'mongoose';

export const relationSchema = new Schema({
  _id: {
    type: Types.ObjectId,
    auto: true,
  },
  author: {
    type: Types.ObjectId,
  },
  target: {
    type: Types.ObjectId,
  },
  age: {
    type: Number,
  },
  type: {
    type: String,
  },
  date: {
    type: Number,
  },
});

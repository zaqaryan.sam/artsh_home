import { Schema, Types } from 'mongoose';

export const postsSchema = new Schema({
  _id: {
    type: Types.ObjectId,
    auto: true,
  },
  author: {
    type: Types.ObjectId,
  },
  text: {
    type: String,
  },
  likes: {
    Type: Number,
  },
  comment: {
    type: Number,
  },
  images: {
    type: [Types.ObjectId],
  },
  videos: {
    type: [Types.ObjectId],
  },
  origin: {
    type: Types.ObjectId,
  },
  parent: {
    type: Types.ObjectId,
  },
});

import { Schema, Types } from 'mongoose';

export const filesSchema = new Schema({
  _id: {
    type: Types.ObjectId,
    auto: true,
  },
  author: {
    type: Types.ObjectId,
  },
  url: {
    type: String,
  },
  type: {
    type: String,
  },
});

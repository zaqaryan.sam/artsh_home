import { ObjectId } from 'bson';
import _ from 'lodash';

export class Base {
  serialize<M, D>(obj: M): D {
    return Object.fromEntries(
      Object.entries(obj)
        .map(([key, value]) => {
          const newKey = _.snakeCase(key);
          switch (true) {
            case Array.isArray(value):
              return [newKey, value.map((item) => {
                if (Object.prototype.toString.call(item) === '[object Object]') {
                  return this.serialize(value);
                }
                return item;
              })];
            case Object.prototype.toString.call(value) === '[object Object]':
              return [newKey, this.serialize(value)];
            case (typeof value === 'string' && value.match(/^[0-9a-fA-F]{24}$/)):
              return [newKey, new ObjectId(value)];
            default:
              return [newKey, value];
          }
        }),
    ) as D;
  }

  deSerialize<D, M>(object: D): M {
    return Object.fromEntries(
      Object.entries(object)
        .map(([key, value]) => {
          const newKey = _.camelCase(key);
          switch (true) {
            case Array.isArray(value):
              return [newKey, value.map((item) => {
                if (Object.prototype.toString.call(item) === '[object Object]') {
                  return this.serialize(value);
                }
                return item;
              })];
            case Object.prototype.toString.call(value) === '[object Object]':
              return [newKey, this.deSerialize(value)];
            case value.constructor
              && value.constructor.name.toLowerCase === ObjectId.name.toLowerCase:
              return [newKey, value.toString()];
            default:
              return [newKey, value];
          }
        }),
    ) as M;
  }
}
